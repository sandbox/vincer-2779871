<?php

/**
 * @file
 * Initialize Midi.js.
 *
 * Adds the .fmidi-wrap class as a wrapper.
 * Load midi.js.
 * Add the midi.js controls and info HTML elements,
 * with a few customizations and classes.
 * Initialize midi.js.
 * The changes are all outside the default template markup,
 * except the .fmidi-wrap class,
 * so I think all of this could eventually be moved back into the module
 * and a new JS file for the initializing script, in a more sophisticated way.
 */
?>
<?php
// Load midi.js.
global $base_url; /* If $base_url is undefined */
drupal_add_js($base_url . '/sites/all/libraries/libtimidity/midi.js', 'external');
?>

<div class="<?php print $classes; ?> fmidi-wrap"<?php print $attributes; ?>>
  <?php if (!$label_hidden): ?>
  <div class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</div>
  <?php endif; ?>
  <div class="field-items"<?php print $content_attributes; ?>>
    <?php foreach ($items as $delta => $item): ?>
    <div class="field-item <?php print $delta % 2 ? 'odd' : 'even'; ?>"<?php print $item_attributes[$delta]; ?>><?php print render($item); ?></div>
    <?php endforeach; ?>
  </div>
  <div id="fmidi-ctrl"><a href="#stopmidi" onClick="MIDIjs.stop();" class="fmidi-stop">Stop MIDI Playback</a></div>
  <div class="fmidi-messages">
    <div id="fmidi-info"><span class="label">MIDI.js status:</span> <span id="MIDIjs.message" class="fmidi-active">Initializing ...</span> <br />
      <span class="label">MIDI.js audio time (s):</span> <span id="MIDIjs.audio_time" class="fmidi-active">-</span></div>
  </div>
  <script type='text/javascript'>

  var message_span = document.getElementById('MIDIjs.message');
  message_span.innerHTML = MIDIjs.get_audio_status();

  MIDIjs.message_callback = display_status;
  function display_status(message) {
     message_span.innerHTML = message;
  };

  var time_span = document.getElementById('MIDIjs.audio_time');
  MIDIjs.player_callback = display_time;
  function display_time(player_event) {
     time_span.innerHTML = player_event.time;
  };

</script>
</div>


CONTENTS OF THIS FILE
---------------------
   
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * FAQ
 * Maintainers
 * Resources


INTRODUCTION
------------

Welcome to FieldMidity. Please see the advanced help for more information.
FieldMidity implements Midi.js and LibTiMidityJS libraries in a file field 
display formatter for Drupal. It plays midi files in your browser, 
using a sound bank, where each voice is downloaded as they are called 
by the channel data in the midi file. 
It's similar to the old midi browser plugins, 
except that the controller is javascript and the samples are hosted remotely.
So, this works with any device that’s Javascript enabled, including mobile
devices.


REQUIREMENTS
------------

This module requires the following javascript libraries:

* Midi.js (Feulner version)
* Libtimidity.js

This module requires the following sequencer patches:

* Arachno soundbank .pat files
* MT32Drum soundbank .pat files


INSTALLATION
------------

* Download LibTiMidityJS:
  https://github.com/fourkitchens/poolparty.fourkitchens.com
  Download the library to your libraries folder 
  and rename the folder to libtimidity.
  Follow the patterns noted below.

$ git clone git://github.com/fourkitchens/poolparty.fourkitchens.com.git

/sites/all/libraries/libtimidity/midi.js
/sites/all/libraries/libtimidity/libtimidity.js
/sites/all/libraries/libtimidity/pat/*arachno-pat-files
/sites/all/libraries/libtimidity/pat/MT32Drums/*mt32drum-pat-files

* Download GUS PAT files:
  https://github.com/babelsberg/babelsberg-js
  https://github.com/babelsberg/babelsberg-js/tree/master/midijs/pat
  Download the full project and pull out the PAT folder. 
  Follow the patterns noted above.
  The PAT directory is 83MB and contains 176 .pat Gravis Ultrasound 
  sample files (129 Arachno, 47 MT32Drum).

$ git clone git://github.com/babelsberg/babelsberg-js.git

* Install as you would normally install a contributed Drupal module. See:
  https://drupal.org/documentation/install/modules-themes/modules-7
  for further information.


CONFIGURATION
-------------

* Add a file field to a content type with the machine name of [midi].
  Set the allowed file extensions to [mid midi].
  Set the field's display mode to Midi.


TROUBLESHOOTING
---------------

* The midi components aren't loading on the front end.

   - Ensure that the machine name of the midi field is [midi]. 
   Otherwise, you would need to copy the midi field template into your theme 
   and rename it to suit your requirements.


FAQ
---

Q: Why do certain channel voices sound weird for certain midi files?

A: Midi files are created under a number of different formats, MT-32 and 
GM being the most basic and most common. This sequencer is conformed to MT-32, 
and as such will reproduce MT-32 midi files with the most accuracy. 
More advanced formats include; GS, XG1, XG2, etc. 
   Also, midi authors could be using a customized setting that doesn't properly 
conform to the given midi specification.


MAINTAINERS
-----------

* Vincent Rosati (vincer) - https://www.drupal.org/user/657362


RESOURCES
---------

* LibTiMidityJS info:
  https://github.com/fourkitchens/poolparty.fourkitchens.com

* Midi.js info:
  http://www.midijs.net/
  http://www.midijs.net/lib/midi.js

* TiMidity++ (libTiMidity) info:
  https://sourceforge.net/projects/timidity/

* Gravis Ultrasound info:
  http://www.gravisultrasound.com/about.htm

* Random midi projects:
  https://mudcu.be/midi-js/

* Arachno info:
  http://www.arachnosoft.com/main/soundfont.php

* Timidity info:
  https://wiki.archlinux.org/index.php/timidity
